// Arrow-aside

let asideButton = document.querySelector('.aside__button');
let aside = document.querySelector('.aside');

asideButton.addEventListener('click', function() {
  aside.classList.toggle('aside--active');
});
