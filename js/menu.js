// Menu

var menuButton = document.querySelector('.menu__button');
var menuList = document.querySelector('.menu__list');

menuButton.addEventListener('click', function() {
  menuList.classList.toggle('menu__list--active');
});
